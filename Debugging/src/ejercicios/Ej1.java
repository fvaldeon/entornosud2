package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 *  
		 */
		
		/*
			La finalidad de declarar las siguientes variables al inicio del main
			es unicamente didáctica, siendo una mala practica en programacion.
			
			Las variables se deben declarar siempre en el momento de usarlas.
			
			En los siguientes ejercicios sobre manejo del Debugger, me interesa
			que podamos ver los cambios en las variables desde el inicio del programa
			
		*/
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		
		for(int i = 0 ; i <= cadenaLeida.length(); i++){
			caracter = cadenaLeida.charAt(i);
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
