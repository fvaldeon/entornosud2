package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		
		/*
			La finalidad de declarar las siguientes variables al inicio del main
			es unicamente didáctica, siendo una mala practica en programacion.
			
			Las variables se deben declarar siempre en el momento de usarlas.
			
			En los siguientes ejercicios sobre manejo del Debugger, me interesa
			que podamos ver los cambios en las variables desde el inicio del programa
			
		*/
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}
		
		input.close();
		
		
		
	}
}
