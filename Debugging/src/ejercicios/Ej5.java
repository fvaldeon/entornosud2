package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		/*
			La finalidad de declarar las siguientes variables al inicio del main
			es unicamente didáctica, siendo una mala practica en programacion.
			
			Las variables se deben declarar siempre en el momento de usarlas.
			
			En los siguientes ejercicios sobre manejo del Debugger, me interesa
			que podamos ver los cambios en las variables desde el inicio del programa
			
		*/
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
