package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instruccion y avanzar
		 * instruccion a instruccion (step into) analizando el contenido de las variables
		 */
		
		
		/*
			La finalidad de declarar las siguientes variables al inicio del main
			es unicamente didáctica, siendo una mala practica en programacion.
			
			Las variables se deben declarar siempre en el momento de usarlas.
			
			En los siguientes ejercicios sobre manejo del Debugger, me interesa
			que podamos ver los cambios en las variables desde el inicio del programa
			
		*/
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i >= 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}

}
